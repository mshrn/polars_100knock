# polars 100knock memo

## `select`と`with_columns`
- psqlの`SELECT`に似た感じで使える。
- selectは抽出、with_columnsはカラムの追加
- カラムに対する演算処理は`pl.col(COLUMNE_NAME)`を通じて実現できる。かなり表現力は高い。
- 列名が重複してはならないので、`pl.col().max()`, `pl.col().min()`を並べるときなどは`.alias()`, `.suffix()`を併用する。

## `filter`
- psqlの`WHERE`に似ている。
- 引数はboolを返すようなもので、`pl.col()`のメソッドを使うとスマートにかける。

## pl.col（）
- 文字列に対する処理、配列に対する処理など色々ある。
- 具体的なメソッドは公式見た方が早い。

## 集約
- `groupby()`と`agg()`でつなぐ。
- `.over()`を使えば`groupby`, `agg`で得られる結果をカラムとして追加できる。

## join
- rdbmsと同じ感覚で使える。ドキュメントを見れば理解できる。

